#!/bin/bash 

#Function to print the beginning of the script 

start(){
	echo -e "${CYAN}\n###########################################################################"
	echo "		Start of the script		"
	echo -e "###########################################################################\n${NC}"
}

#Function to print the end of the script
end(){
	echo -e "${CYAN}\n###########################################################################"
	echo "		End of the script		"
	echo -e "###########################################################################\n${NC}"
}


#Function that will try if the IP is up
#The command for that is the fping -c1 that will send a single packet and see if it's get back
#If the answer is yes i add this ip into a file
#I used a file to stored this information to synchronize the result of the 256 multi subprocess

network_Scan (){

	local value=$1
	local Network_IP_To_Test=$(sed 's/$/'$value'/' <<< $Network_IP)
	fping -c1 -t300 $Network_IP_To_Test 2>/dev/null 1>/dev/null

		if [ "$?" = 0 ]
		then
		echo $Network_IP_To_Test >> .All_IP_File
		fi
}

#Function that use the file create previously with all answering IPs 
#I loop trough the file and add all ip into my array 
#At the end of the loop i removed the file 

file_Reader(){
	lines=$(cat .All_IP_File)
	for line in $lines
	do
		IPs_array+=("$line")
	done
	rm .All_IP_File
}

#Function to print the content of the array with all good IPs
show_Array(){
	
	echo -e "All IPs that are up\n\n"

	for element in "${IPs_array[@]}"
	do
		echo -e "${GREEN}$element${NC}" "IP is used"
	done

}

port_Scanner(){
	
	for port in {1..10}
	do
		timeout 1 bash -c "</dev/tcp/$1/$port &>/dev/null" && echo "port $port is open"
	done

}

Main(){
	start_time=$(date +%s.%3N)
	start
 	
#For loop to check all possible IP in the network 
#The call of the function and the check of the return value is in a different subporcess for all 256 call 
#With this method the time used to control all IP is under 2 seconds 

for i in {1..256}
do
	network_Scan $i &

done

wait 

file_Reader

show_Array

echo -e "\n\nThe script will know search for Hostname and open ports for all IPs found\n\n"

#Call the hostname function and then the ports_scanner function 
end 

end_time=$(date +%s.%3N)

duration=$(echo "scale=3; $end_time - $start_time" | bc)

echo $duration " Time elapsed"

}


#####################################################################

#Main code 
#

#all variables used in the main code 

#Color variables for the output 
RED='\033[0;31m'
GREEN='\033[0;32m'
CYAN='\033[0;36m' 
NC='\033[0m'

#Global variable used in multiple function 
IPs_array=()
IP_Address=$(hostname -I)
Network_IP=$(sed 's![^.]*$!!' <<< $IP_Address) 


Main


